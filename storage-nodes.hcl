job "democratic-csi-nfs-node" {
  datacenters = ["homelab"]

  # you can run node plugins as service jobs as well, but this ensures
  # that all nodes in the DC have a copy.
  type = "system"

  group "nodes" {
    task "plugin" {
      driver = "podman"

      env {
        CSI_NODE_ID = "${attr.unique.hostname}"
      }

      config {
        image = "docker.io/democraticcsi/democratic-csi:latest"

        args = [
          "--csi-version=1.9.0",
          # must match the csi_plugin.id attribute below
          "--csi-name=org.democratic-csi.nfs",
          "--driver-config-file=${NOMAD_TASK_DIR}/driver-config-file.yaml",
          "--log-level=info",
          "--csi-mode=node",
          "--server-socket=/csi/csi.sock",
        ]

        # node plugins must run as privileged jobs because they
        # mount disks to the host
        privileged = true
        network_mode = "host"
      }

      template {
        destination = "${NOMAD_TASK_DIR}/driver-config-file.yaml"

        data = <<EOH
driver: freenas-api-nfs
instance_id:
httpConnection:
  protocol: http
  host: "192.168.100.24"
  port: 80
  # use only 1 of apiKey or username/password
  # if both are present, apiKey is preferred
  # apiKey is only available starting in TrueNAS-12
  apiKey: "{TRUENAS_API_KEY}"
  # username: root
  # password:
  allowInsecure: true
  # use apiVersion 2 for TrueNAS-12 and up (will work on 11.x in some scenarios as well)
  # leave unset for auto-detection
  apiVersion: 2
zfs:
  # can be used to override defaults if necessary
  # the example below is useful for TrueNAS 12
  #cli:
  #  sudoEnabled: true
  #
  #  leave paths unset for auto-detection
  #  paths:
  #    zfs: /usr/local/sbin/zfs
  #    zpool: /usr/local/sbin/zpool
  #    sudo: /usr/local/bin/sudo
  #    chroot: /usr/sbin/chroot
  
  # can be used to set arbitrary values on the dataset/zvol
  # can use handlebars templates with the parameters from the storage class/CO
  #datasetProperties:
  #  "org.freenas:description": "Testing"

  datasetParentName: "pool02/csi/nomad"
  # do NOT make datasetParentName and detachedSnapshotsDatasetParentName overlap
  # they may be siblings, but neither should be nested in the other
  # do NOT comment this option out even if you don't plan to use snapshots, just leave it with dummy value
  detachedSnapshotsDatasetParentName: "pool02/csi/snaps"
  datasetEnableQuotas: true
  datasetEnableReservation: false
  datasetPermissionsMode: "0777"
  datasetPermissionsUser: 3002
  datasetPermissionsGroup: 3002

  # not supported yet
  #datasetPermissionsAcls:
  #- "-m everyone@:full_set:allow"
  #- "-m u:kube:full_set:allow"

nfs:
  shareHost: "192.168.108.12"
  shareAlldirs: false
  shareAllowedHosts: ["192.168.100.10", "192.168.100.11", "192.168.100.12"]
  shareAllowedNetworks: []
  shareMaprootUser: ""
  shareMaprootGroup: ""
  shareMapallUser: "csi"
  shareMapallGroup: "csi"
EOH
      }

      csi_plugin {
        # must match --csi-name arg
        id        = "org.democratic-csi.nfs"
        type      = "node"
        mount_dir = "/csi"
      }

      resources {
        cpu    = 500
        memory = 256
      }
    }
  }
}