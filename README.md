# nomad-csi-plugin


I am using the democratic-csi [plugin](https://github.com/democratic-csi/democratic-csi) with [Nomad](https://www.nomadproject.io/) to store container data on an external [TrueNAS Scale](https://www.truenas.com/truenas-scale/) NAS.